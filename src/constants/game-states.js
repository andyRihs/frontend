export const GAME_STATES = {
    NOT_RUNNING: 0,
    INIT_GAME: 1, // Creating cards
    WAITING: 1,
    RUNNING: 2,
    FINISHED: 3,
    FINISHED_VALIDATE_RESULT: 4
};