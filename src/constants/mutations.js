export const MUTATIONS = {
    INIT: 'INIT', // Init action
    RESET: 'RESET', // Reset action
    CREATE_GAME: 'CREATE_GAME', // Create a new game
    JOIN_GAME: 'JOIN_GAME', // Join into a created game
    START_GAME: 'START_GAME', // Start a game event when queue notifies for game start
    INIT_GAME: 'INIT_GAME', // A game is initiated for a player when cards added. Wait for game start
    BUZZWORD_HIT: 'BUZZWORD_HIT',
    // ADD_CARDS: 'ADD_CARDS',
    GAME_FINISH: 'GAME_FINISH', // Game really finished with validated results
    GAME_FINISH_VALIDATE_RESULT: 'GAME_FINISH_VALIDATE_RESULT', // Game finished, wait for result validation
    LOAD: 'LOAD', // App is loading state
    FINISH_LOAD: 'FINISH_LOAD', // App finished loading state
    PLAYER_JOIN: 'PLAYER_JOIN', // Player joined the game. Notified by message queue
    // PLAYER_LEAVE: 'PLAYER_LEAVE', // Player leaves the game. Notified by message queue
    ERROR: 'ERROR' // Add error
};