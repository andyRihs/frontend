export default Object.freeze({
    EASY: 'EASY',
    MEDIUM: 'MEDIUM',
    HEAVY: 'HEAVY',
});